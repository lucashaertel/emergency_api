const express = require('express');
    
var app = express();
var port = process.env.port || 3000;

require('./routes/emergencyPrintRoutes')(app);

app.listen(port);

console.log('server started on port: ' + port);