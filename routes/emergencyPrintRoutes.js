'use strict';
module.exports = function (app) {
    var emergencyPrint = require('../controllers/emergencyPrintController');

    // emergencyPrint Routes
    app.route('/emergency')
        .post(emergencyPrint.print_map);
};