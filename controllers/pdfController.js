'use strict';
const pdfDocument = require('pdfkit');
const fs = require('fs');
const printer = require('pdf-to-printer');

module.exports = {
    generatePDF: generatePDF
}

function generatePDF(image) {
    var doc = new pdfDocument;
    var fname = new Date().toISOString().replace(/T/, '_').replace(/\..+/, '');
    fname = fname.split("-").join("");
    fname = fname.split(":").join("");
    var pathname = './maps/';
    pathname += fname;
    pathname += '.pdf';
    doc.pipe(fs.createWriteStream(pathname));
    /*
    doc.font('fonts/PalatinoBold.ttf')
       .fontSize(25)
       .text('Some text with an embedded font!', 100, 100);
       */

    doc.image(image, {
        fit: [500, 500],
        align: 'center',
        valign: 'center'
    });

    doc.end();
    console.log('The pdf was saved!');
    var path = require('path');

    printer
        .print(path.resolve(pathname))
        .then(console.log)
        .catch(console.error);

    /*
        var pdfPrinter = require('node-pdf-printer');
        var path = require('path');
        console.log(pdfPrinter.listPrinter());
        const array = [path.resolve(pathname)]
        pdfPrinter.printFiles(array);
    */

    console.log("----------------------------------------------------------------------------");
}

