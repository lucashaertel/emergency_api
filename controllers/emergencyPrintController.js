'use strict';

const fs = require('fs');

var mapCtrl = require('./mapController');
var pdfCtrl = require('./pdfController');

exports.print_map = async function(req, res) {
   console.log(req.query);
   saveLog(req.query);
   var image = await mapCtrl.receiveEmergencyData(req.query.address);
   pdfCtrl.generatePDF(image);
}

function saveLog(query) {
   var fname = new Date().toISOString().replace(/T/, '_').replace(/\..+/, '');
   fname = fname.split("-").join("");
   fname = fname.split(":").join("");

   var pathname = './log/';
   pathname += fname;
   pathname += '.txt';
  
   fs.writeFile(pathname, "---- Log ----", function(err) {
     if(err) {
         return console.log(err);
     }
     var logger = fs.createWriteStream(pathname, {
        flags: 'a' // 'a' means appending (old data will be preserved)
      })
      logger.write('\n');
      var text = 'Beschr Kurz: '
      text += query.descshort != null ? query.descshort : '';
      logger.write(text);
      logger.write('\n');
      text = 'Beschr Lang: ';
      text += query.desclong != null ? query.desclong : '';
      logger.write(text);
      logger.write('\n');
      text = 'Stich Kurz: ';
      text += query.keyshort != null ? query.keyshort : '';
      logger.write(text);
      logger.write('\n');
      text = 'Stich Lang: ';
      text += query.keylong != null ? query.keylong : '';
      logger.write(text);
      logger.write('\n');
      text = 'Stich Text: ';
      text += query.keytext != null ? query.keytext : '';
      logger.write(text);
      logger.write('\n');
      text = 'Längengrad: ';
      text += query.longitude != null ? query.longitude : '';
      logger.write(text);
      logger.write('\n');
      text = 'Breitengrad: ';
      text += query.latitude != null ? query.latitude : '';
      logger.write(text);
      logger.write('\n');
      text = 'Adresse: ';
      text += query.address != null ? query.address : '';
      logger.write(text);
      logger.write('\n');
     console.log("The file was saved!");
 }); 
}