'use strict';
const request = require('request-promise');

const startNumber = '3';
const startStreet = 'Am Rathausplatz';
const startTown = 'Dannstadt';
const markerSize = 'small';
const mapWidth = '1000';
const mapHeight = '1000';
const startLabel = 'S';
const startColor = 'blue';
const destinationLabel = 'D';
const destinationColor = 'red';
const apiKey = 'AIzaSyBu7yxQlthOcDIiLUxGkC3-kU0uODizZ14';
/*
module.exports = {
    receiveEmergencyData: async function (address) {
        var routesUrl = buildRoutesUrl(address);
        var polylineEncodedValue = await getEncodedPolyline(routesUrl);
        var staticMapUrl = builStaticMapURL(address, polylineEncodedValue);
        var image = await processImage(staticMapUrl);
        console.log();
    }
    }
*/
module.exports = {
receiveEmergencyData: receiveEmergencyData
}

async function receiveEmergencyData(address){
    var routesUrl = buildRoutesUrl(address);
    var polylineEncodedValue = await getEncodedPolyline(routesUrl);
    var staticMapUrl = builStaticMapURL(address, polylineEncodedValue);
    var image = await processImage(staticMapUrl);
    return image;
}

function buildRoutesUrl(address) {
    var directionsRequest = 'https://maps.googleapis.com/maps/api/directions/json?';
    // start Adress
    directionsRequest += 'origin=';
    directionsRequest += startNumber;
    directionsRequest += '+';
    directionsRequest += startStreet.replace(' ', '+');
    directionsRequest += ',+';
    directionsRequest += startTown.replace(' ', '+');
    directionsRequest += '&';
    // destinaton adress
    directionsRequest += 'destination=';
    directionsRequest += address.replace(' ', '+');
    directionsRequest += '&';
    // api key
    directionsRequest += 'key=';
    directionsRequest += apiKey;
    return directionsRequest;
}

async function getEncodedPolyline(routesUrl){
    // return request(routesUrl, { json: true }, (err, res, body) => {
    //     return body.routes[0].overview_polyline.points;
    // });
    const response = await request(routesUrl, { json: true })
    return response.routes[0].overview_polyline.points
}


function builStaticMapURL(address, path) {
    var staticMapUrl = 'https://maps.googleapis.com/maps/api/staticmap?';
    // img size
    staticMapUrl += 'size=';
    staticMapUrl += mapWidth;
    staticMapUrl += 'x';
    staticMapUrl += mapHeight;
    staticMapUrl += '&';
    // marker start
    staticMapUrl += 'markers=';
    staticMapUrl += 'size:';
    staticMapUrl += markerSize;
    staticMapUrl += '|';
    staticMapUrl += 'color:';
    staticMapUrl += startColor;
    staticMapUrl += '|';
    staticMapUrl += 'label:';
    staticMapUrl += startLabel;
    staticMapUrl += '|';
    staticMapUrl += startNumber;
    staticMapUrl += '+';
    staticMapUrl += startStreet.replace(" ", "+");
    staticMapUrl += ',+';
    staticMapUrl += startTown.replace(" ", "+");
    staticMapUrl += '&';
    // marker destination
    staticMapUrl += 'markers=';
    staticMapUrl += 'size:';
    staticMapUrl += markerSize;
    staticMapUrl += '|';
    staticMapUrl += 'color:';
    staticMapUrl += destinationColor;
    staticMapUrl += '|';
    staticMapUrl += 'label:';
    staticMapUrl += destinationLabel;
    staticMapUrl += '|';
    staticMapUrl += address.replace(" ", "+");
    staticMapUrl += '&';
    //path
    staticMapUrl += 'path=enc:';
    staticMapUrl += path;
    staticMapUrl += '&';
    //api key
    staticMapUrl += 'key=';
    staticMapUrl += apiKey;
    return staticMapUrl;
}

async function processImage(imgUrl) {

const response = await request(imgUrl, { encoding: null })
return response
    /* var request = require('request').defaults({ encoding: null });
    request.get(url, function (err, res, body) {
      console.log();
    }); */
}